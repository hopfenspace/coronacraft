# Make Macerator use important Grindstone recipes
mods.ic2.Macerator.addRecipe(<appliedenergistics2:material:4>, <minecraft:wheat>);
mods.ic2.Macerator.addRecipe(<appliedenergistics2:material:3>, <ore:gemQuartz>);
mods.ic2.Macerator.addRecipe(<appliedenergistics2:material:2>, <ore:crystalCertusQuartz>);
mods.ic2.Macerator.addRecipe(<appliedenergistics2:material:8>, <ore:crystalFluix>);
