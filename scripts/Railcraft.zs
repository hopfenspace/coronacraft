# Saltpeter
mods.ic2.Macerator.addRecipe(<railcraft:dust:2>, <minecraft:flint>);

# Brass
recipes.addShapeless(<railcraft:ingot:9> * 2, [<ore:ingotBronze>, <ore:ingotBronze>]);

# Nickel
recipes.addShapeless(<railcraft:ingot:6> * 2, [<ore:ingotCopper>, <ore:ingotCopper>]);

# Cathodes
recipes.addShaped(<railcraft:charge:8>, [[<ore:plateTin>], [<ore:plateTin>], [<ore:plateTin>]]);
recipes.addShaped(<railcraft:charge:6>, [[<ore:plateCopper>], [<ore:plateCopper>], [<ore:plateCopper>]]);
recipes.addShaped(<railcraft:charge:7>, [[<ore:plateIron>], [<ore:plateIron>], [<ore:plateIron>]]);
